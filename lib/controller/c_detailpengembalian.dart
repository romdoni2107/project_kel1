import 'package:get/get.dart';
import 'package:project_kel1/model/detail_pengembalian.dart';

class CDetailPengembalian extends GetxController {
  Rx<DetailPengembalian> _datadetailpengembalian = DetailPengembalian().obs;

  DetailPengembalian get user => _datadetailpengembalian.value;

  void setUser(DetailPengembalian datadata_pengembalian_detail) =>
      _datadetailpengembalian.value = datadata_pengembalian_detail;
}
