import 'package:get/get.dart';
import 'package:project_kel1/model/detail_pengajuan.dart';

class CDetailPengajuan extends GetxController {
  Rx<DetailPengajuan> _datadetailpengajuan = DetailPengajuan().obs;

  DetailPengajuan get user => _datadetailpengajuan.value;

  void setUser(DetailPengajuan datadata_pengajuan_detail) =>
      _datadetailpengajuan.value = datadata_pengajuan_detail;
}
