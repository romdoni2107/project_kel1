import 'package:get/get.dart';
import 'package:project_kel1/model/pengembalian.dart';

class CPengembalian extends GetxController {
  Rx<Pengembalian> _datapengembalian = Pengembalian().obs;

  Pengembalian get user => _datapengembalian.value;

  void setUser(Pengembalian datadata_pengembalian) =>
      _datapengembalian.value = datadata_pengembalian;
}
