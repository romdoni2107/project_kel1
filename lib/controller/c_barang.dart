import 'package:get/get.dart';
import 'package:project_kel1/model/barang.dart';

class CBarang extends GetxController {
  Rx<Barang> _databarang = Barang().obs;

  Barang get user => _databarang.value;

  void setUser(Barang datadata_barang) => _databarang.value = datadata_barang;
}
