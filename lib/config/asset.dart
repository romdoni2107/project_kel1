import 'dart:ui';

class Asset {
  static Color colorPrimaryDark = Color.fromARGB(255, 115, 18, 32);
  static Color colorPrimary = Color.fromARGB(224, 211, 8, 39);
  static Color colorSecoundary = Color.fromARGB(255, 255, 124, 1);
  static Color colorAccent = Color.fromARGB(255, 251, 202, 25);
}
