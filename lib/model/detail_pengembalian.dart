class DetailPengembalian {
  String? kodePengembalian;
  String? kodeBarang;
  String? jumlah;

  DetailPengembalian({
    this.kodePengembalian,
    this.kodeBarang,
    this.jumlah,
  });

  factory DetailPengembalian.fromJson(Map<String, dynamic> json) => DetailPengembalian(
        kodePengembalian: json['kode_pengembalian'],
        kodeBarang: json['kode_barang'],
        jumlah: json['jumlah'],
      );

  Map<String, dynamic> toJson() => {
        'kode_pengembalian': this.kodePengembalian,
        'kode_barang': this.kodeBarang,
        'jumlah': this.jumlah,
      };
}
