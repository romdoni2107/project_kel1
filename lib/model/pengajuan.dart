class Pengajuan {
  String? kodePengajuan;
  String? tanggal;
  String? npmPeminjam;
  String? namaPeminjam;
  String? prodi;
  String? noHandphone;

  Pengajuan({
    this.kodePengajuan,
    this.tanggal,
    this.npmPeminjam,
    this.namaPeminjam,
    this.prodi,
    this.noHandphone,
  });

  factory Pengajuan.fromJson(Map<String, dynamic> json) => Pengajuan(
        kodePengajuan: json['kode_pengajuan'],
        tanggal: json['tanggal'],
        npmPeminjam: json['npm_peminjam'],
        namaPeminjam: json['nama_peminjam'],
        prodi: json['prodi'],
        noHandphone: json['no_handphone'],
      );

  Map<String, dynamic> toJson() => {
        'kode_pengajuan': this.kodePengajuan,
        'npm_pengajuan': this.npmPeminjam,
        'nama_peminjam': this.namaPeminjam,
        'prodi': this.prodi,
        'no_handphone': this.noHandphone,
      };
}
