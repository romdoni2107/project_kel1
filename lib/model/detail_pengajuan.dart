class DetailPengajuan {
  String? kodePengajuan;
  String? kodeBarang;
  String? jumlah;

  DetailPengajuan({
    this.kodePengajuan,
    this.kodeBarang,
    this.jumlah,
  });

  factory DetailPengajuan.fromJson(Map<String, dynamic> json) =>
      DetailPengajuan(
        kodePengajuan: json['kode_pengajuan'],
        kodeBarang: json['kode_barang'],
        jumlah: json['jumlah'],
      );

  Map<String, dynamic> toJson() => {
        'kode_pengajuan': this.kodePengajuan,
        'kode_barang': this.kodeBarang,
        'jumlah': this.jumlah,
      };
}
