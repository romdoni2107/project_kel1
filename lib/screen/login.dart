import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:project_kel1/config/asset.dart';
import 'package:project_kel1/event/event_db.dart';
import 'package:project_kel1/screen/admin/add_update_user.dart';
import 'package:project_kel1/screen/admin/dashboard_admin.dart';
import 'package:project_kel1/screen/admin/list_user.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  var _controllerUserName = TextEditingController();
  var _controllerPass = TextEditingController();
  var _formKey = GlobalKey<FormState>();

  bool _isHidden = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: GradientAppBar(
        gradient:
            LinearGradient(colors: [Asset.colorPrimary, Asset.colorSecoundary]),
        title: Text('Universitas Teknokrat Indonesia'), centerTitle: true, toolbarHeight: 10,
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: 10,
            ),
            Container(
              height: 150,
              width: 150,
              child: Image(
                  image: NetworkImage(
                      "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a0/UNIVERSITASTEKNOKRAT.png/480px-UNIVERSITASTEKNOKRAT.png")),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              'Campus Assets',
              style: TextStyle(
                  fontSize: 30,
                  color: Asset.colorPrimary,
                  fontWeight: FontWeight.bold,
                  fontStyle: FontStyle.italic),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              'Silahkan Login',
              style: TextStyle(
                  fontSize: 20,
                  color: Asset.colorSecoundary,
                  fontWeight: FontWeight.bold),
            ),
            Form(
              key: _formKey,
              child: Padding(
                padding: EdgeInsets.all(20),
                child: Column(
                  children: [
                    TextFormField(
                      validator: (value) =>
                          value == '' ? 'Jangan Kosong' : null,
                      controller: _controllerUserName,
                      style: TextStyle(
                        color: Asset.colorPrimary,
                      ),
                      decoration: InputDecoration(
                          hintText: 'username',
                          hintStyle: TextStyle(
                            color: Asset.colorPrimary,
                          ),
                          filled: true,
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(
                              color: Asset.colorPrimary,
                              width: 1,
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(
                              color: Asset.colorPrimary,
                              width: 2,
                            ),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(
                              color: Asset.colorPrimary,
                              width: 1,
                            ),
                          ),
                          prefixIcon: Icon(
                            Icons.people,
                            color: Asset.colorPrimary,
                          )),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    TextFormField(
                      obscureText: _isHidden,
                      validator: (value) =>
                          value == '' ? 'Jangan Kosong' : null,
                      controller: _controllerPass,
                      style: TextStyle(
                        color: Asset.colorPrimary,
                      ),
                      decoration: InputDecoration(
                          hintText: 'password',
                          hintStyle: TextStyle(
                            color: Asset.colorPrimary,
                          ),
                          filled: true,
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(
                              color: Asset.colorPrimary,
                              width: 1,
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(
                              color: Asset.colorPrimary,
                              width: 2,
                            ),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(
                              color: Asset.colorPrimary,
                              width: 1,
                            ),
                          ),
                          prefixIcon: Icon(
                            Icons.lock,
                            color: Asset.colorPrimary,
                          ),
                          suffixIcon: GestureDetector(
                            onTap: () {
                              setState(() {
                                _isHidden = !_isHidden;
                              });
                            },
                            child: _isHidden
                                ? Icon(
                                    Icons.visibility_off,
                                    color: Colors.red,
                                  )
                                : Icon(
                                    Icons.visibility,
                                    color: Colors.blue,
                                  ),
                          )),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Container(
                      child: GestureDetector(
                        onTap: () {
                          // Aksi yang ingin dilakukan saat teks ditekan
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => AddUpdateUser()),
                          );
                          // Misalnya, buka halaman reset password
                          // Navigator.push(context, MaterialPageRoute(builder: (context) => ResetPasswordPage()));
                        },
                        child: RichText(
                          text: TextSpan(
                            text: 'Lupa Password? ',
                            style: TextStyle(color: Colors.black),
                            children: [
                              TextSpan(
                                text: 'Klik Disini',
                                style: TextStyle(
                                  color: Colors.blue,
                                  decoration: TextDecoration.underline,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 14,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        ElevatedButton(
                          child: Text(
                            'Login',
                            style: TextStyle(
                              fontSize: 20,
                            ),
                          ),
                          style: ElevatedButton.styleFrom(
                            primary: Asset.colorPrimary,
                            onPrimary: Colors.white,
                            minimumSize: Size(120, 50),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5)),
                          ),
                          onPressed: () {
                            if (_formKey.currentState!.validate()) {
                              EventDb.login(_controllerUserName.text,
                                  _controllerPass.text);
                              _controllerUserName.clear();
                              _controllerPass.clear();
                            }

                            // Tambahkan logika untuk memeriksa login di sini
                          },
                        ),
                        SizedBox(width: 10),
                        ElevatedButton(
                          child: Text(
                            'Daftar',
                            style: TextStyle(
                              fontSize: 20,
                            ),
                          ),
                          style: ElevatedButton.styleFrom(
                            primary: Asset.colorSecoundary,
                            onPrimary: Colors.white,
                            minimumSize: Size(120, 50),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5)),
                          ),
                          onPressed: () {
                            Navigator.pushNamed(context, '/addUser');
                          },
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: Container(
        height: 25,
        decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: [Asset.colorSecoundary, Asset.colorPrimary])),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'developed by kelompok 1',
              style: TextStyle(
                color: Colors.white,
                fontSize: 12,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
