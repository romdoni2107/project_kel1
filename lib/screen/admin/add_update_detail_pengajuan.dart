import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:get/get.dart';
import 'package:project_kel1/config/asset.dart';
import 'package:project_kel1/event/event_db.dart';
import 'package:project_kel1/model/detail_pengajuan.dart';
import 'package:project_kel1/screen/admin/list_detail_pengajuan.dart';
import 'package:project_kel1/screen/admin/list_pengajuan.dart';
import 'package:project_kel1/widget/info.dart';
import '../../model/pengajuan.dart';

class AddUpdatePengajuanDetail extends StatefulWidget {
  final DetailPengajuan? data_pengajuan_detail;
  AddUpdatePengajuanDetail({this.data_pengajuan_detail});

  @override
  State<AddUpdatePengajuanDetail> createState() => _AddUpdatePengajuanDetailState();
}

class _AddUpdatePengajuanDetailState extends State<AddUpdatePengajuanDetail> {
  var _formKey = GlobalKey<FormState>();
  var _controllerkodePengajuan = TextEditingController();
  var _controllerkodeBarang = TextEditingController();
  var _controllerjumlah = TextEditingController();

  bool _isHidden = true;
  @override
  void initState() {
    // TODO: implement initState
    if (widget.data_pengajuan_detail != null) {
      _controllerkodePengajuan.text = widget.data_pengajuan_detail!.kodePengajuan!;
      _controllerkodeBarang.text = widget.data_pengajuan_detail!.kodeBarang!;
      _controllerjumlah.text = widget.data_pengajuan_detail!.jumlah!;
    }
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        // titleSpacing: 0,
        title: widget.data_pengajuan_detail != null
            ? Text('Update Detail Pengajuan')
            : Text('Tambah Detail Pengajuan'),
        backgroundColor: Asset.colorPrimary,
      ),
      body: Stack(
        children: [
          Form(
            key: _formKey,
            child: ListView(
              padding: EdgeInsets.all(16),
              children: [
                TextFormField(
                  enabled: widget.data_pengajuan_detail == null ? true : false,
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerkodePengajuan,
                  decoration: InputDecoration(
                      labelText: "Kode Pengajuan",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),       
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerkodeBarang,
                  decoration: InputDecoration(
                      labelText: "Kode Barang",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerjumlah,
                  decoration: InputDecoration(
                      labelText: "Jumlah",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      if (widget.data_pengajuan_detail == null) {
                        String message = await EventDb.AddPengajuanDetail(
                          _controllerkodePengajuan.text,
                          _controllerkodeBarang.text,
                          _controllerjumlah.text,
                        );
                        Info.snackbar(message);
                        if (message.contains('Berhasil')) {
                          _controllerkodePengajuan.clear();
                          _controllerkodeBarang.clear();
                          _controllerjumlah.clear();
                          Get.off(
                            ListDetailPengajuan(),
                          );
                        }
                      } else {
                        EventDb.UpdatePengajuanDetail(
                          _controllerkodePengajuan.text,
                          _controllerkodeBarang.text,
                          _controllerjumlah.text,
                        );
                      }
                    }
                  },
                  child: Text(
                    widget.data_pengajuan_detail == null ? 'Simpan' : 'Ubah',
                    style: TextStyle(fontSize: 16),
                  ),
                  style: ElevatedButton.styleFrom(
                      primary: Asset.colorSecoundary,
                      fixedSize: Size.fromHeight(50),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5))),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
