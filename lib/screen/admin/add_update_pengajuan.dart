import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:get/get.dart';
import 'package:project_kel1/config/asset.dart';
import 'package:project_kel1/event/event_db.dart';
import 'package:project_kel1/screen/admin/list_pengajuan.dart';
import 'package:project_kel1/widget/info.dart';
import '../../model/pengajuan.dart';

class AddUpdatePengajuan extends StatefulWidget {
  final Pengajuan? data_pengajuan;
  AddUpdatePengajuan({this.data_pengajuan});

  @override
  State<AddUpdatePengajuan> createState() => _AddUpdatePengajuanState();
}

class _AddUpdatePengajuanState extends State<AddUpdatePengajuan> {
  var _formKey = GlobalKey<FormState>();
  var _controllerkodePengajuan = TextEditingController();
  var _controllertanggal = TextEditingController();
  var _controllernpmPeminjam = TextEditingController();
  var _controllernamaPeminjam = TextEditingController();
  var _controllerprodi = TextEditingController();
  var _controllernoHandphone = TextEditingController();

  bool _isHidden = true;
  @override
  void initState() {
    // TODO: implement initState
    if (widget.data_pengajuan != null) {
      _controllerkodePengajuan.text = widget.data_pengajuan!.kodePengajuan!;
      _controllertanggal.text = widget.data_pengajuan!.tanggal!;
      _controllernpmPeminjam.text = widget.data_pengajuan!.npmPeminjam!;
      _controllernamaPeminjam.text = widget.data_pengajuan!.namaPeminjam!;
      _controllerprodi.text = widget.data_pengajuan!.prodi!;
      _controllernoHandphone.text = widget.data_pengajuan!.noHandphone!;
    }
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        // titleSpacing: 0,
        title: widget.data_pengajuan != null
            ? Text('Update Pengajuan')
            : Text('Tambah Pengajuan'),
        backgroundColor: Asset.colorPrimary,
      ),
      body: Stack(
        children: [
          Form(
            key: _formKey,
            child: ListView(
              padding: EdgeInsets.all(16),
              children: [
                TextFormField(
                  enabled: widget.data_pengajuan == null ? true : false,
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerkodePengajuan,
                  decoration: InputDecoration(
                      labelText: "Kode Pengajuan",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  enabled: widget.data_pengajuan == null ? true : false,
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllertanggal,
                  decoration: InputDecoration(
                    labelText: 'Tanggal',
                    hintText: 'Pilih Tanggal',
                    prefixIcon: Icon(Icons.calendar_today),
                    iconColor: Asset.colorSecoundary,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  onTap: () {
                    showDatePicker(
                      context: context,
                      initialDate: DateTime.now(),
                      firstDate: DateTime(2000),
                      lastDate: DateTime(2100),
                    ).then((selectedDate) {
                      if (selectedDate != null) {
                        String formattedDate =
                            DateFormat('yyyy-MM-dd').format(selectedDate);
                        // Lakukan sesuatu dengan tanggal yang dipilih
                        // Misalnya, simpan ke variabel atau lakukan validasi
                        setState(() {
                          _controllertanggal.text =
                              formattedDate; // Set nilai teks di TextFormField
                        });
                      }
                    });
                  },
                  readOnly: true,
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllernpmPeminjam,
                  decoration: InputDecoration(
                      labelText: "Npm Peminjam",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllernamaPeminjam,
                  decoration: InputDecoration(
                      labelText: "Nama Peminjam",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerprodi,
                  decoration: InputDecoration(
                      labelText: "Program Studi",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllernoHandphone,
                  decoration: InputDecoration(
                      labelText: "No Handphone",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      if (widget.data_pengajuan == null) {
                        String message = await EventDb.AddPengajuan(
                          _controllerkodePengajuan.text,
                          _controllertanggal.text,
                          _controllernpmPeminjam.text,
                          _controllernamaPeminjam.text,
                          _controllerprodi.text,
                          _controllernoHandphone.text,
                        );
                        Info.snackbar(message);
                        if (message.contains('Berhasil')) {
                          _controllerkodePengajuan.clear();
                          _controllertanggal.clear();
                          _controllernpmPeminjam.clear();
                          _controllernamaPeminjam.clear();
                          _controllerprodi.clear();
                          _controllernoHandphone.clear();
                          Get.off(
                            ListPengajuan(),
                          );
                        }
                      } else {
                        EventDb.UpdatePengajuan(
                          _controllerkodePengajuan.text,
                          _controllertanggal.text,
                          _controllernpmPeminjam.text,
                          _controllernamaPeminjam.text,
                          _controllerprodi.text,
                          _controllernoHandphone.text,
                        );
                      }
                    }
                  },
                  child: Text(
                    widget.data_pengajuan == null ? 'Simpan' : 'Ubah',
                    style: TextStyle(fontSize: 16),
                  ),
                  style: ElevatedButton.styleFrom(
                      primary: Asset.colorSecoundary,
                      fixedSize: Size.fromHeight(50),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5))),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
