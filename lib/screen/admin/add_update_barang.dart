import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:get/get.dart';
import 'package:project_kel1/config/asset.dart';
import 'package:project_kel1/event/event_db.dart';
import 'package:project_kel1/screen/admin/list_barang.dart';
import 'package:project_kel1/widget/info.dart';
import '../../model/barang.dart';

class AddUpdateBarang extends StatefulWidget {
  final Barang? data_barang;
  AddUpdateBarang({this.data_barang});

  @override
  State<AddUpdateBarang> createState() => _AddUpdateBarangState();
}

class _AddUpdateBarangState extends State<AddUpdateBarang> {
  var _formKey = GlobalKey<FormState>();
  var _controllerkodeBarang = TextEditingController();
  var _controllernamaBarang = TextEditingController();
  var _controllerjumlah = TextEditingController();

  bool _isHidden = true;
  @override
  void initState() {
    // TODO: implement initState
    if (widget.data_barang != null) {
      _controllerkodeBarang.text = widget.data_barang!.kodeBarang!;
      _controllernamaBarang.text = widget.data_barang!.namaBarang!;
      _controllerjumlah.text = widget.data_barang!.jumlah!;
    }
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        // titleSpacing: 0,
        title: widget.data_barang != null
            ? Text('Update Barang')
            : Text('Tambah Barang'),
        backgroundColor: Asset.colorPrimary,
      ),
      body: Stack(
        children: [
          Form(
            key: _formKey,
            child: ListView(
              padding: EdgeInsets.all(16),
              children: [
                TextFormField(
                  enabled: widget.data_barang == null ? true : false,
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerkodeBarang,
                  decoration: InputDecoration(
                      labelText: "Kode Barang",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllernamaBarang,
                  decoration: InputDecoration(
                      labelText: "Nama Barang",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerjumlah,
                  decoration: InputDecoration(
                      labelText: "Jumlah",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      if (widget.data_barang == null) {
                        String message = await EventDb.AddBarang(
                          _controllerkodeBarang.text,
                          _controllernamaBarang.text,
                          _controllerjumlah.text,
                        );
                        Info.snackbar(message);
                        if (message.contains('Berhasil')) {
                          _controllerkodeBarang.clear();
                          _controllernamaBarang.clear();
                          _controllerjumlah.clear();
                          Get.off(
                            ListBarang(),
                          );
                        }
                      } else {
                        EventDb.UpdateBarang(
                          _controllerkodeBarang.text,
                          _controllernamaBarang.text,
                          _controllerjumlah.text,
                        );
                      }
                    }
                  },
                  child: Text(
                    widget.data_barang == null ? 'Simpan' : 'Ubah',
                    style: TextStyle(fontSize: 16),
                  ),
                  style: ElevatedButton.styleFrom(
                      primary: Asset.colorSecoundary,
                      fixedSize: Size.fromHeight(50),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5))),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
