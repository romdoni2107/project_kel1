import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/src/dialog/dialog_route.dart';
import 'package:project_kel1/config/asset.dart';
import 'package:project_kel1/event/event_db.dart';
import 'package:project_kel1/model/detail_pengembalian.dart';
import 'package:project_kel1/screen/admin/add_update_detail_pengembalian.dart';
import 'package:project_kel1/screen/admin/list_user.dart';
// import 'package:project_kelas/screen/admin/add_update_pengembalian.dart';

class ListDetailPengembalian extends StatefulWidget {
  @override
  State<ListDetailPengembalian> createState() => _ListDetailPengembalianState();
}

class _ListDetailPengembalianState extends State<ListDetailPengembalian> {
  List<DetailPengembalian> _listDetailPengembalian = [];

  void getPengembalianDetail() async {
    _listDetailPengembalian = await EventDb.getPengembalianDetail();

    setState(() {});
  }

  @override
  void initState() {
    getPengembalianDetail();
    super.initState();
  }

  void showOption(DetailPengembalian? data_pengembalian_detail) async {
    var result = await Get.dialog(
        SimpleDialog(
          children: [
            ListTile(
              onTap: () => Get.back(result: 'update'),
              title: Text('Update'),
            ),
            ListTile(
              onTap: () => Get.back(result: 'delete'),
              title: Text('Delete'),
            ),
            ListTile(
              onTap: () => Get.back(),
              title: Text('Close'),
            )
          ],
        ),
        barrierDismissible: false);
    switch (result) {
      case 'update':
        Get.to(AddUpdatePengembalianDetail(
                data_pengembalian_detail: data_pengembalian_detail))
            ?.then((value) => getPengembalianDetail());
        break;
      case 'delete':
        EventDb.deletePengembalianDetail(
                data_pengembalian_detail!.kodePengembalian!)
            .then((value) => getPengembalianDetail());
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: GradientAppBar(
        gradient:
            LinearGradient(colors: [Asset.colorPrimary, Asset.colorSecoundary]),
        // titleSpacing: 0,
        title: Text('List Detail Pengembalian'), centerTitle: true,
        toolbarHeight: 10,
      ),
      body: Stack(
        children: [
          _listDetailPengembalian.length > 0
              ? ListView.builder(
                  itemCount: _listDetailPengembalian.length,
                  itemBuilder: (context, index) {
                    DetailPengembalian detailPengembalian =
                        _listDetailPengembalian[index];
                    return ListTile(
                      leading: CircleAvatar(
                        child: Text('${index + 1}'),
                        backgroundColor: Colors.white,
                      ),
                      title: Text(detailPengembalian.kodeBarang ?? ''),
                      subtitle: Text(detailPengembalian.jumlah ?? ''),
                      trailing: IconButton(
                          onPressed: () => showOption(detailPengembalian),
                          icon: Icon(Icons.more_vert)),
                    );
                  },
                )
              : Center(
                  child: Text("Data Kosong"),
                ),
          Positioned(
            bottom: 16,
            right: 16,
            child: FloatingActionButton(
              onPressed: () => Get.to(AddUpdatePengembalianDetail())
                  ?.then((value) => getPengembalianDetail()),
              child: Icon(Icons.add),
              backgroundColor: Asset.colorAccent,
            ),
          )
        ],
      ),
    );
  }
}
