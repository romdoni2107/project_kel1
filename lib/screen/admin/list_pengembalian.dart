import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/src/dialog/dialog_route.dart';
import 'package:project_kel1/config/asset.dart';
import 'package:project_kel1/event/event_db.dart';
import 'package:project_kel1/model/pengembalian.dart';
import 'package:project_kel1/model/pengembalian.dart';
import 'package:project_kel1/screen/admin/add_update_pengembalian.dart';
import 'package:project_kel1/screen/admin/list_detail_pengembalian.dart';
import 'package:project_kel1/screen/admin/list_user.dart';
// import 'package:project_kelas/screen/admin/add_update_pengembalian.dart';

class ListPengembalian extends StatefulWidget {
  @override
  State<ListPengembalian> createState() => _ListPengembalianState();
}

class _ListPengembalianState extends State<ListPengembalian> {
  List<Pengembalian> _listPengembalian = [];

  void getPengembalian() async {
    _listPengembalian = await EventDb.getPengembalian();

    setState(() {});
  }

  @override
  void initState() {
    getPengembalian();
    super.initState();
  }

  void showOption(Pengembalian? data_pengembalian) async {
    var result = await Get.dialog(
        SimpleDialog(
          children: [
            ListTile(
              onTap: () => Get.back(result: 'update'),
              title: Text('Update'),
            ),
            ListTile(
              onTap: () => Get.back(result: 'delete'),
              title: Text('Delete'),
            ),
            ListTile(
              onTap: () => Get.back(),
              title: Text('Close'),
            )
          ],
        ),
        barrierDismissible: false);
    switch (result) {
      case 'update':
        Get.to(AddUpdatePengembalian(data_pengembalian: data_pengembalian))
            ?.then((value) => getPengembalian());
        break;
      case 'delete':
        EventDb.deletePengembalian(data_pengembalian!.kodePengembalian!)
            .then((value) => getPengembalian());
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: GradientAppBar(
        gradient:
            LinearGradient(colors: [Asset.colorPrimary, Asset.colorSecoundary]),
        // titleSpacing: 0,
        title: Text('List Pengembalian'), centerTitle: true, toolbarHeight: 10,
      ),
      body: Stack(
        children: [
          _listPengembalian.length > 0
              ? ListView.builder(
                  itemCount: _listPengembalian.length,
                  itemBuilder: (context, index) {
                    Pengembalian pengembalian = _listPengembalian[index];
                    return ListTile(
                      leading: CircleAvatar(
                        child: Text('${index + 1}'),
                        backgroundColor: Colors.white,
                      ),
                      title: Text(pengembalian.kodePengembalian ?? ''),
                      subtitle: Text(pengembalian.tanggalKembali ?? ''),
                      trailing: IconButton(
                          onPressed: () => showOption(pengembalian),
                          icon: Icon(Icons.more_vert)),
                    );
                  },
                )
              : Center(
                  child: Text("Data Kosong"),
                ),
          Positioned(
            bottom: 16,
            left: 350,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                FloatingActionButton(
                  onPressed: () => Get.to(AddUpdatePengembalian())
                      ?.then((value) => getPengembalian()),
                  child: Icon(Icons.add),
                  backgroundColor: Asset.colorAccent,
                ),
                SizedBox(height: 8),
                ElevatedButton(
                  onPressed: () {
                    Get.off(
                        ListDetailPengembalian()); // Aksi yang akan dilakukan ketika tombol "Detail Pengajuan" ditekan
                  },
                  style: ElevatedButton.styleFrom(
                    primary: Asset
                        .colorPrimaryDark, // Ganti warna latar belakang tombol dengan warna biru
                    // properti lain yang dapat disesuaikan seperti padding, shape, dst.
                  ),
                  child: Text('Detail Pengembalian'),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
