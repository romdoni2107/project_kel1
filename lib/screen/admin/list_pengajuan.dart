import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/src/dialog/dialog_route.dart';
import 'package:project_kel1/config/asset.dart';
import 'package:project_kel1/event/event_db.dart';
import 'package:project_kel1/model/pengajuan.dart';
import 'package:project_kel1/model/pengajuan.dart';
import 'package:project_kel1/screen/admin/add_update_pengajuan.dart';
import 'package:project_kel1/screen/admin/list_detail_pengajuan.dart';
import 'package:project_kel1/screen/admin/list_user.dart';
// import 'package:project_kelas/screen/admin/add_update_pengajuan.dart';

class ListPengajuan extends StatefulWidget {
  @override
  State<ListPengajuan> createState() => _ListPengajuanState();
}

class _ListPengajuanState extends State<ListPengajuan> {
  List<Pengajuan> _listPengajuan = [];

  void getPengajuan() async {
    _listPengajuan = await EventDb.getPengajuan();

    setState(() {});
  }

  @override
  void initState() {
    getPengajuan();
    super.initState();
  }

  void showOption(Pengajuan? data_pengajuan) async {
    var result = await Get.dialog(
        SimpleDialog(
          children: [
            ListTile(
              onTap: () => Get.back(result: 'update'),
              title: Text('Update'),
            ),
            ListTile(
              onTap: () => Get.back(result: 'delete'),
              title: Text('Delete'),
            ),
            ListTile(
              onTap: () => Get.back(),
              title: Text('Close'),
            )
          ],
        ),
        barrierDismissible: false);
    switch (result) {
      case 'update':
        Get.to(AddUpdatePengajuan(data_pengajuan: data_pengajuan))
            ?.then((value) => getPengajuan());
        break;
      case 'delete':
        EventDb.deletePengajuan(data_pengajuan!.kodePengajuan!)
            .then((value) => getPengajuan());
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: GradientAppBar(
        gradient:
            LinearGradient(colors: [Asset.colorPrimary, Asset.colorSecoundary]),
        // titleSpacing: 0,
        title: Text('List Pengajuan'), centerTitle: true, toolbarHeight: 10,
      ),
      body: Stack(
        children: [
          _listPengajuan.length > 0
              ? ListView.builder(
                  itemCount: _listPengajuan.length,
                  itemBuilder: (context, index) {
                    Pengajuan pengajuan = _listPengajuan[index];
                    return ListTile(
                      leading: CircleAvatar(
                        child: Text('${index + 1}'),
                        backgroundColor: Colors.white,
                      ),
                      title: Text(pengajuan.namaPeminjam ?? ''),
                      subtitle: Text(pengajuan.npmPeminjam ?? ''),
                      trailing: IconButton(
                          onPressed: () => showOption(pengajuan),
                          icon: Icon(Icons.more_vert)),
                    );
                  },
                )
              : Center(
                  child: Text("Data Kosong"),
                ),
          Positioned(
            bottom: 16,
            left: 350,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                FloatingActionButton(
                  onPressed: () => Get.to(AddUpdatePengajuan())
                      ?.then((value) => getPengajuan()),
                  child: Icon(Icons.add),
                  backgroundColor: Asset.colorAccent,
                ),
                SizedBox(height: 8),
                ElevatedButton(
                  onPressed: () {
                    Get.off(
                        ListDetailPengajuan()); // Aksi yang akan dilakukan ketika tombol "Detail Pengajuan" ditekan
                  },
                  style: ElevatedButton.styleFrom(
                    primary: Asset
                        .colorPrimaryDark, // Ganti warna latar belakang tombol dengan warna biru
                    // properti lain yang dapat disesuaikan seperti padding, shape, dst.
                  ),
                  child: Text('Detail Pengajuan'),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
