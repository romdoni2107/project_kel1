import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/src/dialog/dialog_route.dart';
import 'package:project_kel1/config/asset.dart';
import 'package:project_kel1/event/event_db.dart';
import 'package:project_kel1/model/detail_pengajuan.dart';
import 'package:project_kel1/model/pengajuan.dart';
import 'package:project_kel1/model/pengajuan.dart';
import 'package:project_kel1/screen/admin/add_update_detail_pengajuan.dart';
import 'package:project_kel1/screen/admin/add_update_pengajuan.dart';
import 'package:project_kel1/screen/admin/list_pengajuan.dart';
import 'package:project_kel1/screen/admin/list_user.dart';
// import 'package:project_kelas/screen/admin/add_update_pengajuan.dart';

class ListDetailPengajuan extends StatefulWidget {
  @override
  State<ListDetailPengajuan> createState() => _ListDetailPengajuanState();
}

class _ListDetailPengajuanState extends State<ListDetailPengajuan> {
  List<DetailPengajuan> _listDetailPengajuan = [];

  void getPengajuanDetail() async {
    _listDetailPengajuan = await EventDb.getPengajuanDetail();

    setState(() {});
  }

  @override
  void initState() {
    getPengajuanDetail();
    super.initState();
  }

  void showOption(DetailPengajuan? data_pengajuan_detail) async {
    var result = await Get.dialog(
        SimpleDialog(
          children: [
            ListTile(
              onTap: () => Get.back(result: 'update'),
              title: Text('Update'),
            ),
            ListTile(
              onTap: () => Get.back(result: 'delete'),
              title: Text('Delete'),
            ),
            ListTile(
              onTap: () => Get.back(),
              title: Text('Close'),
            )
          ],
        ),
        barrierDismissible: false);
    switch (result) {
      case 'update':
        Get.to(AddUpdatePengajuanDetail(data_pengajuan_detail: data_pengajuan_detail))
            ?.then((value) => getPengajuanDetail());
        break;
      case 'delete':
        EventDb.deletePengajuanDetail(data_pengajuan_detail!.kodePengajuan!)
            .then((value) => getPengajuanDetail());
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: GradientAppBar(
        gradient:
            LinearGradient(colors: [Asset.colorPrimary, Asset.colorSecoundary]),
        // titleSpacing: 0,
        title: Text('List Detail Pengajuan'), centerTitle: true, toolbarHeight: 10,
      ),
      body: Stack(
        children: [
          _listDetailPengajuan.length > 0
              ? ListView.builder(
                  itemCount: _listDetailPengajuan.length,
                  itemBuilder: (context, index) {
                    DetailPengajuan detailPengajuan = _listDetailPengajuan[index];
                    return ListTile(
                      leading: CircleAvatar(
                        child: Text('${index + 1}'),
                        backgroundColor: Colors.white,
                      ),
                      title: Text(detailPengajuan.kodeBarang?? ''),
                      subtitle: Text(detailPengajuan.jumlah ?? ''),
                      trailing: IconButton(
                          onPressed: () => showOption(detailPengajuan),
                          icon: Icon(Icons.more_vert)),
                    );
                  },
                )
              : Center(
                  child: Text("Data Kosong"),
                ),
          Positioned(
            bottom: 16,
            right: 16,
            child: FloatingActionButton(
              onPressed: () =>
                  Get.to(AddUpdatePengajuanDetail())?.then((value) => getPengajuanDetail()),
              child: Icon(Icons.add),
              backgroundColor: Asset.colorAccent,
            ),
          )
        ],
      ),
    );
  }
}
