import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:get/get.dart';
import 'package:project_kel1/config/asset.dart';
import 'package:project_kel1/event/event_db.dart';
import 'package:project_kel1/model/pengembalian.dart';
import 'package:project_kel1/screen/admin/list_pengembalian.dart';
import 'package:project_kel1/widget/info.dart';
import '../../model/pengembalian.dart';

class AddUpdatePengembalian extends StatefulWidget {
  final Pengembalian? data_pengembalian;
  AddUpdatePengembalian({this.data_pengembalian});

  @override
  State<AddUpdatePengembalian> createState() => _AddUpdatePengembalianState();
}

class _AddUpdatePengembalianState extends State<AddUpdatePengembalian> {
  var _formKey = GlobalKey<FormState>();
  var _controllerkodePengembalian = TextEditingController();
  var _controllerkodePengajuan = TextEditingController();
  var _controllertanggalKembali = TextEditingController();

  bool _isHidden = true;
  @override
  void initState() {
    // TODO: implement initState
    if (widget.data_pengembalian != null) {
      _controllerkodePengembalian.text =
          widget.data_pengembalian!.kodePengembalian!;
      _controllerkodePengajuan.text = widget.data_pengembalian!.kodePengajuan!;
      _controllertanggalKembali.text =
          widget.data_pengembalian!.tanggalKembali!;
    }
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        // titleSpacing: 0,
        title: widget.data_pengembalian != null
            ? Text('Update Pengembalian')
            : Text('Tambah Pengembalian'),
        backgroundColor: Asset.colorPrimary,
      ),
      body: Stack(
        children: [
          Form(
            key: _formKey,
            child: ListView(
              padding: EdgeInsets.all(16),
              children: [
                TextFormField(
                  enabled: widget.data_pengembalian == null ? true : false,
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerkodePengembalian,
                  decoration: InputDecoration(
                      labelText: "Kode Pengembalian",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerkodePengajuan,
                  decoration: InputDecoration(
                      labelText: "Kode Pengajuan",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllertanggalKembali,
                  decoration: InputDecoration(
                    labelText: 'Tanggal',
                    hintText: 'Pilih Tanggal',
                    prefixIcon: Icon(Icons.calendar_today),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  onTap: () {
                    showDatePicker(
                      context: context,
                      initialDate: DateTime.now(),
                      firstDate: DateTime(2000),
                      lastDate: DateTime(2100),
                    ).then((selectedDate) {
                      if (selectedDate != null) {
                        String formattedDate =
                            DateFormat('yyyy-MM-dd').format(selectedDate);
                        // Lakukan sesuatu dengan tanggal yang dipilih
                        // Misalnya, simpan ke variabel atau lakukan validasi
                        setState(() {
                          _controllertanggalKembali.text =
                              formattedDate; // Set nilai teks di TextFormField
                        });
                      }
                    });
                  },
                  readOnly: true,
                ),
                SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      if (widget.data_pengembalian == null) {
                        String message = await EventDb.AddPengembalian(
                          _controllerkodePengembalian.text,
                          _controllerkodePengajuan.text,
                          _controllertanggalKembali.text,
                        );
                        Info.snackbar(message);
                        if (message.contains('Berhasil')) {
                          _controllerkodePengembalian.clear();
                          _controllerkodePengajuan.clear();
                          _controllertanggalKembali.clear();
                          Get.off(
                            ListPengembalian(),
                          );
                        }
                      } else {
                        EventDb.UpdatePengembalian(
                          _controllerkodePengembalian.text,
                          _controllerkodePengajuan.text,
                          _controllertanggalKembali.text,
                        );
                      }
                    }
                  },
                  child: Text(
                    widget.data_pengembalian == null ? 'Simpan' : 'Ubah',
                    style: TextStyle(fontSize: 16),
                  ),
                  style: ElevatedButton.styleFrom(
                      primary: Asset.colorSecoundary,
                      fixedSize: Size.fromHeight(50),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5))),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
