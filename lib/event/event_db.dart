import 'dart:convert';
import 'package:get/get.dart';
import 'package:get/get_connect/http/src/utils/utils.dart';
import 'package:project_kel1/config/api.dart';
import 'package:project_kel1/event/event_pref.dart';
import 'package:project_kel1/model/barang.dart';
import 'package:project_kel1/model/detail_pengajuan.dart';
import 'package:project_kel1/model/detail_pengembalian.dart';
import 'package:project_kel1/model/pengajuan.dart';
import 'package:project_kel1/model/pengembalian.dart';
import 'package:project_kel1/model/user.dart';
import 'package:http/http.dart' as http;
import 'package:project_kel1/screen/admin/add_update_detail_pengajuan.dart';
import 'package:project_kel1/screen/admin/add_update_pengajuan.dart';
import 'package:project_kel1/widget/info.dart';

import '../screen/admin/dashboard_admin.dart';

class EventDb {
  static Future<User?> login(String username, String pass) async {
    User? user;

    try {
      var response = await http.post(Uri.parse(Api.login), body: {
        'username': username,
        'pass': pass,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);

        if (responBody['success']) {
          user = User.fromJson(responBody['user']);
          EventPref.saveUser(user);
          Info.snackbar('Login Berhasil');
          Future.delayed(Duration(milliseconds: 1700), () {
            Get.off(
              DashboardAdmin(),
            );
          });
        } else {
          Info.snackbar('Username dan Password Salah');
        }
      } else {
        Info.snackbar('Request Username dan Password Salah');
      }
    } catch (e) {
      print(e);
    }
    return user;
  }

  static Future<List<User>> getUser() async {
    List<User> listUser = [];

    try {
      var response = await http.get(Uri.parse(Api.getUsers));

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          var user = responBody['user'];

          user.forEach((user) {
            listUser.add(User.fromJson(user));
          });
        }
      }
    } catch (e) {
      print(e);
    }

    return listUser;
  }

  static Future<String> addUser(
      String name, String username, String pass, String role) async {
    String reason;

    try {
      var response = await http.post(Uri.parse(Api.addUser), body: {
        'name': name,
        'username': username,
        'pass': pass,
        'role': role
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          reason = 'Add User Berhasil';
        } else {
          reason = responBody['reason'];
        }
      } else {
        reason = "Request Gagal";
      }
    } catch (e) {
      print(e);
      reason = e.toString();
    }

    return reason;
  }

  static Future<void> UpdateUser(
    String id_user,
    String name,
    String username,
    String pass,
    String role,
  ) async {
    try {
      var response = await http.post(Uri.parse(Api.updateUser), body: {
        'id_user': id_user,
        'name': name,
        'username': username,
        'pass': pass,
        'role': role
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Update User');
        } else {
          Info.snackbar('Gagal Update User');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<void> deleteUser(String id_user) async {
    try {
      var response = await http
          .post(Uri.parse(Api.deleteUser), body: {'id_user': id_user});

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Delete User');
        } else {
          Info.snackbar('Gagal Delete User');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<List<Barang>> getBarang() async {
    List<Barang> listBarang = [];

    try {
      var response = await http.get(Uri.parse(Api.getBarang));

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          var barang = responBody['data_barang'];

          barang.forEach((data_barang) {
            listBarang.add(Barang.fromJson(data_barang));
          });
        }
      }
    } catch (e) {
      print(e);
    }

    return listBarang;
  }

  static Future<String> AddBarang(
      String kode_barang, String nama_barang, String jumlah) async {
    String reason;

    try {
      var response = await http.post(Uri.parse(Api.addBarang), body: {
        'kode_barang': kode_barang,
        'nama_barang': nama_barang,
        'jumlah': jumlah
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          reason = 'Add Data Barang Berhasil';
        } else {
          reason = responBody['reason'];
        }
      } else {
        reason = "Request Gagal";
      }
    } catch (e) {
      print(e);
      reason = e.toString();
    }

    return reason;
  }

  static Future<void> UpdateBarang(
      String kode_barang, String nama_barang, String jumlah) async {
    try {
      var response = await http.post(Uri.parse(Api.updateBarang), body: {
        'kode_barang': kode_barang,
        'nama_barang': nama_barang,
        'jumlah': jumlah
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Update Data Barang');
        } else {
          Info.snackbar('Gagal Update Data Barang');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<void> deleteBarang(String kode_barang) async {
    try {
      var response = await http.post(Uri.parse(Api.deleteBarang),
          body: {'kode_barang': kode_barang});

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Delete Data Barang');
        } else {
          Info.snackbar('Gagal Delete Data Barang');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<List<Pengajuan>> getPengajuan() async {
    List<Pengajuan> listPengajuan = [];

    try {
      var response = await http.get(Uri.parse(Api.getPengajuan));

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          var pengajuan = responBody['data_pengajuan'];

          pengajuan.forEach((data_pengajuan) {
            listPengajuan.add(Pengajuan.fromJson(data_pengajuan));
          });
        }
      }
    } catch (e) {
      print(e);
    }

    return listPengajuan;
  }

  static Future<String> AddPengajuan(
      String kode_pengajuan,
      String tanggal,
      String npm_peminjam,
      String nama_peminjam,
      String prodi,
      String no_handphone) async {
    String reason;

    try {
      var response = await http.post(Uri.parse(Api.addPengajuan), body: {
        'kode_pengajuan': kode_pengajuan,
        'tanggal': tanggal,
        'npm_peminjam': npm_peminjam,
        'nama_peminjam': nama_peminjam,
        'prodi': prodi,
        'no_handphone': no_handphone
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          reason = 'Add Data Pengajuan Berhasil';
        } else {
          reason = responBody['reason'];
        }
      } else {
        reason = "Request Gagal";
      }
    } catch (e) {
      print(e);
      reason = e.toString();
    }

    return reason;
  }

  static Future<void> UpdatePengajuan(
      String kode_pengajuan,
      String tanggal,
      String npm_peminjam,
      String nama_peminjam,
      String prodi,
      String no_handphone) async {
    try {
      var response = await http.post(Uri.parse(Api.updatePengajuan), body: {
        'kode_pengajuan': kode_pengajuan,
        'tanggal': tanggal,
        'npm_peminjam': npm_peminjam,
        'nama_peminjam': nama_peminjam,
        'prodi': prodi,
        'no_handphone': no_handphone
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Update Data Pengajuan');
        } else {
          Info.snackbar('Gagal Update Data Pengajuan');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<void> deletePengajuan(String kode_pengajuan) async {
    try {
      var response = await http.post(Uri.parse(Api.deletePengajuan),
          body: {'kode_pengajuan': kode_pengajuan});

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Delete Data Pengajuan');
        } else {
          Info.snackbar('Gagal Delete Data Pengajuan');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<List<Pengembalian>> getPengembalian() async {
    List<Pengembalian> listPengembalian = [];

    try {
      var response = await http.get(Uri.parse(Api.getPengembalian));

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          var pengembalian = responBody['data_pengembalian'];

          pengembalian.forEach((data_pengembalian) {
            listPengembalian.add(Pengembalian.fromJson(data_pengembalian));
          });
        }
      }
    } catch (e) {
      print(e);
    }

    return listPengembalian;
  }

  static Future<String> AddPengembalian(String kode_pengembalian,
      String kode_pengajuan, String tanggal_kembali) async {
    String reason;

    try {
      var response = await http.post(Uri.parse(Api.addPengembalian), body: {
        'kode_pengembalian': kode_pengembalian,
        'kode_pengajuan': kode_pengajuan,
        'tanggal_kembali': tanggal_kembali,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          reason = 'Add Data Pengembalian Berhasil';
        } else {
          reason = responBody['reason'];
        }
      } else {
        reason = "Request Gagal";
      }
    } catch (e) {
      print(e);
      reason = e.toString();
    }

    return reason;
  }

  static Future<void> UpdatePengembalian(String kode_pengembalian,
      String kode_pengajuan, String tanggal_kembali) async {
    try {
      var response = await http.post(Uri.parse(Api.updatePengembalian), body: {
        'kode_pengembalian': kode_pengembalian,
        'kode_pengajuan': kode_pengajuan,
        'tanggal_kembali': tanggal_kembali,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Update Data Pengembalian');
        } else {
          Info.snackbar('Gagal Update Data Pengembalian');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<void> deletePengembalian(String kode_pengembalian) async {
    try {
      var response = await http.post(Uri.parse(Api.deletePengembalian),
          body: {'kode_pengembalian': kode_pengembalian});

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Delete Data Pengembalian');
        } else {
          Info.snackbar('Gagal Delete Data Pengembalian');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<List<DetailPengembalian>> getPengembalianDetail() async {
    List<DetailPengembalian> listDetailPengembalian = [];

    try {
      var response = await http.get(Uri.parse(Api.getPengembalianDetail));

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          var pengembalianDetail = responBody['data_pengembalian_detail'];

          pengembalianDetail.forEach((data_pengembalian_detail) {
            listDetailPengembalian
                .add(DetailPengembalian.fromJson(data_pengembalian_detail));
          });
        }
      }
    } catch (e) {
      print(e);
    }

    return listDetailPengembalian;
  }

  static Future<String> AddPengembalianDetail(
      String kode_pengembalian, String kode_barang, String jumlah) async {
    String reason;

    try {
      var response = await http.post(Uri.parse(Api.addPengembalian), body: {
        'kode_pengembalian': kode_pengembalian,
        'kode_barang': kode_barang,
        'jumlah': jumlah,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          reason = 'Add Data Detail Pengembalian Berhasil';
        } else {
          reason = responBody['reason'];
        }
      } else {
        reason = "Request Gagal";
      }
    } catch (e) {
      print(e);
      reason = e.toString();
    }

    return reason;
  }

  static Future<void> UpdatePengembalianDetail(
      String kode_pengembalian, String kode_barang, String jumlah) async {
    try {
      var response =
          await http.post(Uri.parse(Api.updatePengembalianDetail), body: {
        'kode_pengembalian': kode_pengembalian,
        'kode_barang': kode_barang,
        'jumlah': jumlah,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Update Data Detail Pengembalian');
        } else {
          Info.snackbar('Gagal Update Data Detail Pengembalian');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<void> deletePengembalianDetail(String kode_pengembalian) async {
    try {
      var response = await http.post(Uri.parse(Api.deletePengembalianDetail),
          body: {'kode_pengembalian': kode_pengembalian});

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Delete Data Detail Pengembalian');
        } else {
          Info.snackbar('Gagal Delete Data Detail Pengembalian');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<List<DetailPengajuan>> getPengajuanDetail() async {
    List<DetailPengajuan> listDetailPengajuan = [];

    try {
      var response = await http.get(Uri.parse(Api.getPengajuanDetail));

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          var pengajuanDetail = responBody['data_pengajuan_detail'];

          pengajuanDetail.forEach((data_pengajuan_detail) {
            listDetailPengajuan
                .add(DetailPengajuan.fromJson(data_pengajuan_detail));
          });
        }
      }
    } catch (e) {
      print(e);
    }

    return listDetailPengajuan;
  }

  static Future<String> AddPengajuanDetail(
      String kode_pengajuan, String kode_barang, String jumlah) async {
    String reason;

    try {
      var response = await http.post(Uri.parse(Api.addPengajuanDetail), body: {
        'kode_pengajuan': kode_pengajuan,
        'kode_barang': kode_barang,
        'jumlah': jumlah,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          reason = 'Add Data Detail Pengajuan Berhasil';
        } else {
          reason = responBody['reason'];
        }
      } else {
        reason = "Request Gagal";
      }
    } catch (e) {
      print(e);
      reason = e.toString();
    }

    return reason;
  }

  static Future<void> UpdatePengajuanDetail(
      String kode_pengajuan, String kode_barang, String jumlah) async {
    try {
      var response =
          await http.post(Uri.parse(Api.updatePengajuanDetail), body: {
        'kode_pengajuan': kode_pengajuan,
        'kode_barang': kode_barang,
        'jumlah': jumlah,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Update Data Detail Pengajuan');
        } else {
          Info.snackbar('Gagal Update Data Detail Pengajuan');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<void> deletePengajuanDetail(String kode_pengajuan) async {
    try {
      var response = await http.post(Uri.parse(Api.deletePengajuanDetail),
          body: {'kode_pengajuan': kode_pengajuan});

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Delete Data Detail Pengajuan');
        } else {
          Info.snackbar('Gagal Delete Data Detail Pengajuan');
        }
      }
    } catch (e) {
      print(e);
    }
  }
}
